
import QtQuick 2.0
import Sailfish.Silica 1.0
//import Sailfish.Contacts 1.0
//import Sailfish.Telephony 1.0
//import org.nemomobile.contacts 1.0
import io.thp.pyotherside 1.3

Page {
    id: callsDetailsPage
    property var var_CallList: []
    onVar_CallListChanged: {
        for(var i = 0; i < var_CallList.length; i++){
            var newdata
            var month = Qt.formatDateTime(new Date(var_CallList[i][2],parseInt(var_CallList[i][1]),var_CallList[i][0]), "MMMM")
            if(var_CallList.length === 7){
                newdata = {"date":Qt.formatDateTime(new Date(var_CallList[i][2],var_CallList[i][1],var_CallList[i][0]), "d") + " de " + month + " de " + var_CallList[i][2],
                    "hour": var_CallList[i][3],
                    "destinationPhoneNumber": var_CallList[i][4],
                    "carrier": var_CallList[i][5],
                    "cost": var_CallList[i][6]+" €",
                    "duration": "Message"}
            }
            else{
                var duration = parseInt(var_CallList[i][7])
                var minutes = Math.floor(duration/60)
                var seconds_str = String(duration-minutes*60)
                if(seconds_str.length === 1){
                    seconds_str = "0"+seconds_str
                }
                var str_duration = String(minutes)+":"+seconds_str
                newdata = {"date":Qt.formatDateTime(new Date(var_CallList[i][2],var_CallList[i][1],var_CallList[i][0]), "d") + " de " + month + " de " + var_CallList[i][2],
                    "hour": var_CallList[i][3],
                    "destinationPhoneNumber": var_CallList[i][4],
                    "carrier": var_CallList[i][5],
                    "cost": var_CallList[i][6]+" €",
                    "duration": str_duration}
            }

            dataModel.append(newdata)
        }
    }

    Column {
        id:column
        width: callsDetailsPage.width
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: Theme.paddingSmall
//        visible: false
        PageHeader {
            id:pageHeader
            title: qsTr("Calls details")
        }
        SilicaListView {
            id: listView
            width: parent.width
            height: callsDetailsPage.height-pageHeader.height
            clip: true
            ScrollDecorator {}
            model: ListModel{
                id: dataModel
            }
            section{
                id: subTitle
                delegate: Column{
                    id: bigColumn
                    width: parent.width*0.95
                    anchors.horizontalCenter: parent.horizontalCenter
                    Item{
                        id: zeroRow
                        height: Theme.itemSizeExtraSmall
                        width: parent.width
                        Label{
                            //anchors.right: parent.right
                            anchors.left: parent.left
                            anchors.verticalCenter: parent.verticalCenter
                            id: date
                            text: section
                            font.pixelSize: Theme.fontSizeMedium
                            color: Theme.highlightColor
                        }
                    }
                }
                property: "date"
            }
            delegate: Column{
                id: bigColumn
                width: parent.width*0.95
                anchors.horizontalCenter: parent.horizontalCenter
                property var date: var_CallList[index][0] + " - " + var_CallList[index][1] + " - " + var_CallList[index][2]
                Item{
                    id: itemMaster
                    width: parent.width
                    height: Theme.itemSizeExtraSmall/3 + Theme.itemSizeMedium
                    Row{
                        id: firstRow
                        height: Theme.itemSizeMedium/2
                        anchors.top: parent.top
                        Item{
                            id: nameItem
                            height: parent.height
                            width: itemMaster.width*2/3
                            Label{
                                id: name
                                width: parent.width
                                //                                    text: "Federico Antonio Ramirez de los Santos"
                                text: ""
                                wrapMode: Text.WordWrap
                                font.pixelSize: Theme.fontSizeExtraSmall
                                color: Theme.highlightColor
                            }
                        }
                        Column{
                            id: phoneColumn
                            width: itemMaster.width - nameItem.width
                            Label{
                                id: phoneNumber
                                anchors.right: parent.right
                                //                                    text: "+0034 123 456 789"
                                //text: var_CallList[index][4]
                                text: destinationPhoneNumber
                                font.pixelSize: Theme.fontSizeExtraSmall
                            }
                            Label{
                                id: phoneCompany
                                anchors.right: parent.right
                                anchors.rightMargin: Theme.paddingMedium
                                //                                    text: "Movistar ES"
                                //text: var_CallList[index][5]
                                text: carrier
                                font.pixelSize: Theme.fontSizeTiny
                                color: Theme.secondaryColor
                            }
                        }
                    }
                    Row{
                        id: secondRow
                        height: Theme.itemSizeMedium/2
                        anchors.top: firstRow.bottom
                        Item{
                            id: callTimeItem
                            height: parent.height
                            width: itemMaster.width/3
                            IconButton{
                                id: callTimeIcon
                                icon.source: "image://theme/icon-s-time"
                                anchors.left: callTime.right
                                anchors.leftMargin: -1.5*Theme.paddingMedium
                                anchors.verticalCenter: callTime.verticalCenter
                            }
                            Label{
                                id: callTime
                                anchors.centerIn: parent
                                //                                    text: "00:23:45"
                                //text: var_CallList[index][3]
                                text: hour
                                font.pixelSize: Theme.fontSizeExtraSmall
                                color: Theme.secondaryHighlightColor
                            }
                        }
                        Item{
                            id: callDurationItem
                            height: parent.height
                            width: itemMaster.width/3
                            IconButton{
                                id: callDurationIcon
                                icon.source: "image://theme/icon-s-duration"
                                anchors.left: callDuration.right
                                anchors.leftMargin: -1.5*Theme.paddingMedium
                                anchors.verticalCenter: callDuration.verticalCenter
                            }
                            Label{
                                id: callDuration
                                anchors.centerIn: parent
                                //                                    text: "134:43"
                                text: duration
                                font.pixelSize: Theme.fontSizeExtraSmall
                                color: Theme.secondaryHighlightColor
                            }
                        }
                        Item{
                            id: callCostItem
                            height: parent.height
                            width: itemMaster.width/3
                            Label{
                                id: callCost
                                anchors.centerIn: parent
                                //                                    text: "99.00"+" €"
                                //text: var_CallList[index][6] + ' €'
                                text: cost
                                font.pixelSize: Theme.fontSizeExtraSmall
                                color: Theme.secondaryHighlightColor
                            }
                        }
                    }
                    Item{
                        width: parent.width
                        height: Theme.itemSizeExtraSmall/3
                    }
                }
            }
        }
    }
    Python{
        id:pythonCallsDetails
        Component.onCompleted: {
            setHandler('CallsDetails',function(CallList){
                var_CallList = CallList;
            });
        }
        onReceived:
            {
                // All the stuff you send, not assigned to a 'setHandler', will be shown here:
                console.log('got message from python: ' + data);
            }
    }
}
