#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pyotherside
from pepelib import Pepe

mypepe = Pepe()

def Login(username, password):
    status = mypepe.DirectLogin(username, password)
    pyotherside.send('loginStatus', status)
    Start()
    pyotherside.send('basicsStatus', True)
    return 1

def Start():
    return mypepe.RequestServices()

def RequestLineList():
    return mypepe._Hub.GetAllPhoneNumbers()

def RequestLineListObjs():
    return mypepe._Hub.GetAllLines()

def RequestBasics(ThePhoneNumber):
    print("Running RequestBasics...")
    TheLine = mypepe._Hub.Get(PhoneNumber = ThePhoneNumber)
    TheList = TheLine.ReturnLineBasics()
    if pyotherside.send('Basics', TheList):
        print("Running RequestBasics... Done")
        return 1
    else:
        print("Running RequestBasics...Failed")
        return 0
    
def GetAllBasics():
    print("Running GetAllBasics...")
    LineList = RequestLineListObjs()
    TheList = []
    for line in LineList:
        TheList.append(line.ReturnLineBasics())
    pyotherside.send('AllLinesBasics',TheList)
    #print(TheList)
    print("Running GetAllBasics...Done")
    return 1

def GetDetailsFor(ThePhoneNumber, InitDate, EndDate):
    TheLine = mypepe._Hub.Get(PhoneNumber = ThePhoneNumber)
    if TheLine.DownloadCallsDetails(InitDate, EndDate):
        CallList = TheLine.ReturnCallsDetails()
        MsgsList = TheLine.ReturnMsgsDetails()
        FullList = CallList + MsgsList
        print(FullList)
        if pyotherside.send("CallsDetails", FullList):
            print("Details sent for", ThePhoneNumber, "between", InitDate, "and", EndDate)
            return 1
        else:
            print("Error sending details for", ThePhoneNumber, "between", InitDate, "and", EndDate)
            return 0
        return 1
    else:
        print("Error getting details for", ThePhoneNumber, "between", InitDate, "and", EndDate)
        return 0




