/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.3
//import Sailfish.Contacts 1.0
//import Sailfish.Telephony 1.0
//import org.nemomobile.contacts 1.0

Page {
    id: loginPage

    property var python: mainpython
    property bool modules_unloaded: true
    property int var_loginStatus: -1

    function resetButtonState(){
        loginButton.text = qsTr("Login")
        if (fieldUser.text !== "" & fieldPassword.text !== ""){
            loginButton.enabled = true
        }
        else{
            loginButton.enabled = false
        }
    }

    function enter(){
        console.log('Login status: '+var_loginStatus)
        if(var_loginStatus == 1){
            logintimer.running = false
            pageStack.push(Qt.resolvedUrl("MainPage.qml"))
        }
        else if (var_loginStatus == 2){
//                        Error in password or email
            logintimer.running = false
            // Set loginButton to initial state
            resetButtonState();
            // Show fields like there is an error
            fieldPassword.errorHighlight = true
            fieldUser.errorHighlight = true
        }
        else if (var_loginStatus == 3 || var_loginStatus == 0){
            logintimer.running = false
            resetButtonState();
//                        Connection error
        }
    }

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent
        PullDownMenu {
//            MenuItem{
//                text: "Testing"
//                onClicked: {
//                    console.log(JSON.stringify())
//                }
//            }
            MenuItem {
                text: "Testing calls details"
                onClicked: pageStack.push(Qt.resolvedUrl("CallsDetails.qml"))
            }
            MenuItem {
                text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("About.qml"))
            }
        }
        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height
        Column {
            anchors.horizontalCenter: parent.horizontalCenter
            id: column
            width: parent.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Login")
            }
            Image {
                id: logoImg
                source:"qrc:///res/icon.png"
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label{
                text:qsTr("PepeFish is an unofficial Pepephone client")
//                width: 0.7*parent.width
                color: Theme.secondaryColor
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeSmall
                wrapMode: Text.Wrap
            }
//            Rectangle{
//                color:'red'
//                height: 10
//                width: 0.7*parent.width
//                anchors.horizontalCenter: parent.horizontalCenter
//            }

            TextField {
                id: fieldUser
                inputMethodHints: Qt.ImhEmailCharactersOnly
                placeholderText: qsTr("Enter your email here")
                label: qsTr("Email")
                width: 0.95*parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: fieldPassword.focus = true
                onTextChanged: {
                    errorHighlight = false
                    fieldPassword.errorHighlight = false
                }
            }
            PasswordField {
                id: fieldPassword
                placeholderText: qsTr("And here your password")
                label: qsTr("Password")
                width: 0.95*parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                EnterKey.onClicked: loginButton.focus = true
                onTextChanged: {
                    errorHighlight = false
                    fieldUser.errorHighlight = false
                }
            }
//            TextSwitch{
//                text: qsTr("Save credentials and make automatic login next time")
//                description: qsTr("Credentials will be saved in plain text. NOT RECOMMENDED!")
//                width: 0.95*parent.width
//                checked: false
//                enabled: false // To be changed when implemented
//            }
            Button{
                id:loginButton
                anchors.horizontalCenter: parent.horizontalCenter
                text:qsTr("Log in!")
                enabled: {
                    if(fieldUser.text !== "" & fieldPassword.text !== ""){
                        return true
                    }
                    else{
                        return false
                    }
                }
                onClicked: {
                    enabled = false
                    text = qsTr("Login...")
                    var_loginStatus = -1
                    python.call('conector.Login', [fieldUser.text, fieldPassword.text] ,function(Username, Password) {});
                    logintimer.running = true
                    console.log('Login button clicked!')
                }
            }
            Timer{
                id: logintimer
                running: false
                repeat: true
                interval: 100
                onTriggered: {
                    enter();
                }
            }
        }
    }
    Python{
        id:pythonLogin
        Component.onCompleted: {
            setHandler('loginStatus',function(loginStatus){
                var_loginStatus = loginStatus;
            });
        }
        function start(){
        call('conector.Start', function() {});
    }

        onReceived:
            {
                // All the stuff you send, not assigned to a 'setHandler', will be shown here:
                console.log('got message from python: ' + data);
            }
    }
}


