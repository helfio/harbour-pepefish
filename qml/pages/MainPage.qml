import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.3

Page {
    id: mainPage
    property var all_lines_basics: []
    property var var_CallList_details: []
    property bool var_bascisStatus: false
    backNavigation: false

    onVar_bascisStatusChanged: {
        pythonMainPage.getAllBasics();
        console.log("Basic status changed.")
    }

    SilicaFlickable{
        anchors.fill: parent
        VerticalScrollDecorator{}
        PullDownMenu{
            MenuItem {
                text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("About.qml"))
            }
            MenuItem{
                text:qsTr("Refresh info")
                onClicked: {
                    pythonMainPage.getAllBasics();
                }
            }
        }
        Column{
            id: mainColumn
            width: parent.width
            PageHeader {
                title: qsTr("Basic information")
            }
            Repeater{
                model: all_lines_basics.length
                Column {
                    id:datacolumn
                    width: parent.width*0.9
                    anchors.horizontalCenter: parent.horizontalCenter
                    spacing: Theme.itemSizeExtraSmall/5
                    ListItem {
                        width: mainColumn.width
                        anchors.horizontalCenter: parent.horizontalCenter
//                        height: (phoneAlias.height + phoneNumber.height)*1.1
                        Item{
                            height: parent.height
                            width: mainColumn.width
                            anchors.centerIn: parent
                            Label{
                                id: phoneAlias
                                color: Theme.primaryColor
                                anchors.top: parent.top
                                anchors.right: parent.right
                                anchors.rightMargin: Theme.paddingMedium
                                font.pixelSize: Theme.fontSizeLarge
                                text: all_lines_basics[index][0]
                            }
                            Label{
                                id: phoneNumber
                                color: Theme.secondaryHighlightColor
                                anchors.top: phoneAlias.bottom
                                anchors.right: parent.right
                                anchors.rightMargin: Theme.paddingLarge
                                font.pixelSize: Theme.fontSizeSmall
                                text: all_lines_basics[index][1]
                            }
                        }
                        menu: ContextMenu {
                            MenuItem {
                                text: qsTr("Calls details")
                                property var today : new Date()
                                property var thisDay: Qt.formatDateTime(new Date(), "d/M/yyyy")
                                property var firstOfThisMonth : Qt.formatDateTime(new Date(today.getFullYear(), today.getMonth(), 1), "d/M/yyyy")
                                onClicked: {
                                    pythonMainPage.call('conector.GetDetailsFor', [all_lines_basics[index][1], firstOfThisMonth, thisDay] , function(ThePhoneNumber, InitDate, EndDate) {});
//                                    pageStack.push(Qt.resolvedUrl("CallsDetails.qml"), {var_CallList : var_CallList_details})
                                    pageStack.push("CallsDetails.qml",{thePhoneNumber: String(all_lines_basics[index][1])})
                                }
                            }
                        }
                    }
                    Item{
                        width: parent.width
                        height: Theme.itemSizeExtraSmall/2
                        Label{
                            anchors.right: parent.horizontalCenter
                            anchors.rightMargin: Theme.itemSizeExtraSmall/10
                            text: qsTr("Status")
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                        }
                        Label{
                            anchors.left: parent.horizontalCenter
                            color: Theme.secondaryHighlightColor
                            font.pixelSize: Theme.fontSizeSmall
                            text: if(all_lines_basics[index][2] === "A"){
                                      return qsTr("Active")
                                  }
                                  else{
                                      return qsTr("Inactive")
                                  }
                        }
                    }
                    Item{
                        width: parent.width
//                        height: Theme.itemSizeExtraSmall/2
                        implicitHeight: Math.max(bankLabel.height, bankValue.height)
                        Label{
                            id:bankLabel
                            anchors.right: parent.horizontalCenter
                            anchors.rightMargin: Theme.itemSizeExtraSmall/10
                            text: qsTr("Bank account")
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                        }
                        Label{
                            id:bankValue
                            anchors.left: parent.horizontalCenter
                            color: Theme.secondaryHighlightColor
                            font.pixelSize: Theme.fontSizeSmall
                            width: parent.width/2
                            wrapMode: Text.Wrap
                            text: all_lines_basics[index][3]
                        }
                    }
                    Item{
                        width: parent.width
                        height: Theme.itemSizeExtraSmall/2
                        Label{
                            anchors.right: parent.horizontalCenter
                            anchors.rightMargin: Theme.itemSizeExtraSmall/10
                            text: qsTr("Contract or prepay")
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                        }
                        Label{
                            anchors.left: parent.horizontalCenter
                            color: Theme.secondaryHighlightColor
                            font.pixelSize: Theme.fontSizeSmall
                            text: if (all_lines_basics[index][4] === "POS"){
                                      return qsTr("Contract")
                                  }
                                  else{
                                      return qsTr("Prepay")
                                  }
                        }
                    }
                    Item{
                        width: parent.width
//                        height: Theme.itemSizeExtraSmall/2
                        implicitHeight: Math.max(planLabel.height, planValue.height)
                        Label{
                            id:planLabel
                            anchors.right: parent.horizontalCenter
                            anchors.rightMargin: Theme.itemSizeExtraSmall/10
                            text: qsTr("Plan")
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                        }
                        Label{
                            id:planValue
                            anchors.left: parent.horizontalCenter
                            color: Theme.secondaryHighlightColor
                            font.pixelSize: Theme.fontSizeSmall
                            width: parent.width/2
                            wrapMode: Text.Wrap
                            text: all_lines_basics[index][14]
                        }
                    }
                    Item{
                        width: parent.width
                        height: Theme.itemSizeExtraSmall/2
                        visible: {
                            if (all_lines_basics[index][4] === "POS"){
                                return false
                            }
                            else{
                                return true
                            }
                        }

                        Label{
                            anchors.right: parent.horizontalCenter
                            anchors.rightMargin: Theme.itemSizeExtraSmall/10
                            text: qsTr("Balance")
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                        }
                        Label{
                            anchors.left: parent.horizontalCenter
                            color: Theme.secondaryHighlightColor
                            font.pixelSize: Theme.fontSizeSmall
                            text: all_lines_basics[index][13]
                        }
                    }
                    Item{
                        width: parent.width
                        height: Theme.itemSizeExtraSmall/2
                        Label{
                            anchors.right: parent.horizontalCenter
                            anchors.rightMargin: Theme.itemSizeExtraSmall/10
                            text: qsTr("Data plan price")
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                        }
                        Label{
                            anchors.left: parent.horizontalCenter
                            color: Theme.secondaryHighlightColor
                            font.pixelSize: Theme.fontSizeSmall
                            text: all_lines_basics[index][6]+" €"
                        }
                    }
                    Item{
                        width: parent.width
                        height: Theme.itemSizeExtraSmall/2
                        Label{
                            anchors.right: parent.horizontalCenter
                            anchors.rightMargin: Theme.itemSizeExtraSmall/10
                            text: qsTr("Data plan consumed")
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                        }
                        Label{
                            anchors.left: parent.horizontalCenter
                            color: Theme.secondaryHighlightColor
                            font.pixelSize: Theme.fontSizeSmall
                            text: all_lines_basics[index][9]+" / "+all_lines_basics[index][8]+" Mb"
                        }
                    }
                    Item{
                        width: parent.width
                        height: Theme.itemSizeExtraSmall/2
                        Label{
                            anchors.right: parent.horizontalCenter
                            anchors.rightMargin: Theme.itemSizeExtraSmall/10
                            text: qsTr("Percentage consumed")
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                        }
                        Rectangle{
                            anchors.left: parent.horizontalCenter
                            width: parent.width/2
                            height: parent.height
                            color: 'transparent'
                            border.color: Theme.secondaryColor
                            border.width: Theme.itemSizeExtraSmall/50
                            Rectangle{
                                anchors.left: parent.left
                                color: Theme.secondaryHighlightColor
                                height: parent.height
                                width: (parseFloat(all_lines_basics[index][10])/100)*parent.width
                            }
                            Label{
                                anchors.horizontalCenter: parent.horizontalCenter
                                color: Theme.primaryColor
                                font.pixelSize: Theme.fontSizeSmall
                                text:all_lines_basics[index][10] +" %"
                            }
                        }
                    }
                    Item{
                        width: parent.width
                        height: Theme.itemSizeExtraSmall/2
                        Label{
                            anchors.right: parent.horizontalCenter
                            anchors.rightMargin: Theme.itemSizeExtraSmall/10
                            text: qsTr("Month passed")
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                        }
                        Rectangle{
                            anchors.left: parent.horizontalCenter
                            width: parent.width/2
                            height: parent.height
                            color: 'transparent'
                            border.color: Theme.secondaryColor
                            border.width: Theme.itemSizeExtraSmall/50
                            property var today : new Date()
                            property int month_size : Qt.formatDateTime(new Date(today.getFullYear(), today.getMonth(), 0),"d")
                            property real month_passed : Math.floor(Qt.formatDateTime(new Date(), "d")*10000/month_size)/100;
                            Rectangle{
                                anchors.left: parent.left
                                color: Theme.secondaryHighlightColor
                                height: parent.height
                                width: (parent.month_passed/100)*parent.width
                            }
                            Label{
                                anchors.horizontalCenter: parent.horizontalCenter
                                color: Theme.primaryColor
                                font.pixelSize: Theme.fontSizeSmall
                                text: parent.month_passed +" %"
                            }
                        }
                    }
                    Item{
                        width: parent.width
                        height: Theme.itemSizeExtraSmall/2
                        Label{
                            anchors.right: parent.horizontalCenter
                            anchors.rightMargin: Theme.itemSizeExtraSmall/10
                            text: qsTr("Call duration")
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                        }
                        Label{
                            anchors.left: parent.horizontalCenter
                            color: Theme.secondaryHighlightColor
                            font.pixelSize: Theme.fontSizeSmall
                            text: {
//                                var date = new Date();
//                                date.setSeconds(parseFloat(all_lines_basics[index][11]))
//                                return date.toLocaleTimeString(null,Locale.ShortFormat)

                                var t = parseFloat(all_lines_basics[index][11])
                                var t_h = Math.floor(t/3600)
                                var t_m = Math.floor(t/60 - t_h*60)
                                var t_s = t - t_h*3600 - t_m*60
                                return t_h+":"+t_m+":"+t_s
                            }
                        }
                    }
                    Item{
                        width: parent.width
                        height: Theme.itemSizeExtraSmall/2
                        Label{
                            anchors.right: parent.horizontalCenter
                            anchors.rightMargin: Theme.itemSizeExtraSmall/10
                            text: qsTr("Calls costs")
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                        }
                        Label{
                            anchors.left: parent.horizontalCenter
                            color: Theme.secondaryHighlightColor
                            text: all_lines_basics[index][12]+" €"
                            font.pixelSize: Theme.fontSizeSmall
                        }
                    }
                    Item{
                        width: parent.width
                        height: Theme.itemSizeExtraSmall/2
                        Label{
                            anchors.right: parent.horizontalCenter
                            anchors.rightMargin: Theme.itemSizeExtraSmall/10
                            text: qsTr("Total expenses")
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                        }
                        Label{
                            anchors.left: parent.horizontalCenter
                            color: Theme.secondaryHighlightColor
                            text: all_lines_basics[index][15]+" €"
                            font.pixelSize: Theme.fontSizeSmall
                        }
                    }
                    Item{
                        // Just a blank space
                        width: parent.width
                        height: Theme.itemSizeSmall
                    }
                }
            }
        }
        BusyIndicator{
            anchors.centerIn: parent
            size: BusyIndicatorSize.Large
            running: all_lines_basics === []
        }
    }
    Python{
        id:pythonMainPage
        Component.onCompleted: {
            setHandler('basicsStatus',function(basicsStatus){
                var_bascisStatus = basicsStatus;
                console.log("Update on Basics Status.")
            });
            setHandler('AllLinesBasics',function(basiclist){
                all_lines_basics = basiclist;
                console.log("Handler defined.")
            });
//            setHandler('CallList',function(CallList){
//                var_CallList = CallList;
//            });
        }

        function getAllBasics(){
            call('conector.GetAllBasics', function() {});
            console.log("GetAllBasics defined.")
    }
//        function getDetails(){
//            pythonMainPage.call('conector.GetDetailsFor', [] , function(ThePhoneNumber, InitDate, EndDate) {});
//            console.log("Details requested.")
//        }
        onReceived:
            {
                // All the stuff you send, not assigned to a 'setHandler', will be shown here:
                console.log('got message from python: ' + data);
            }
    }
}





