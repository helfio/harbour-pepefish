#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import xml.etree.ElementTree as etree

class SMS:
  def __init__(self, DateHour, DestinationPhoneNumber, Carrier, Cost):
    self._Day = DateHour.split()[0].split("/")[0]
    self._Month = DateHour.split()[0].split("/")[1]
    self._Year = DateHour.split()[0].split("/")[2]
    self._Hour = DateHour.split()[1]
    self._DestinationPhoneNumber = DestinationPhoneNumber
    self._Carrier = Carrier
    self._Cost = Cost
    
class Call(SMS):
  def __init__(self, DateHour, DestinationPhoneNumber, Carrier, Cost, Duration):
    super().__init__(DateHour, DestinationPhoneNumber, Carrier, Cost)
    self._Duration = Duration
    
class CallDetails:
  def __init__(self, rootElement):
    self._CallTotalDuration = rootElement[0][1].text
    self._CallCost = rootElement[0][0].text.replace(".",",")
    self._OtherCost = rootElement[0][2].text.replace(".",",")
    self._TotalCost = str(float(self._CallCost)+float(self._OtherCost))
    
    self._ListCalls = []
    self._ListOthers = [] # SMS and MMS?
    
    for child in rootElement:
      if child.tag == "llamada":
        if child.attrib['tiplla'] == "SMS" or child.attrib['tiplla'] == "MMS":
          self._ListOthers.append(SMS(child.attrib['fecha'], 
                                      child.attrib['msisdn'], 
                                      child.attrib['opedst'], 
                                      child.attrib['implla']))
        elif child.attrib['tiplla'] == "":
          # Calls
          self._ListCalls.append(Call(child.attrib['fecha'], 
                                      child.attrib['msisdn'], 
                                      child.attrib['opedst'], 
                                      child.attrib['implla'], 
                                      child.attrib['seglla'])) # Segundos
      else:
        pass
      
  
  def ShowListCalls(self):
    outputList = []
    for call in self._ListCalls:
      outputList.append([call._Day, 
                         call._Month, 
                         call._Year, 
                         call._Hour, 
                         call._DestinationPhoneNumber,
                         call._Carrier,
                         call._Cost,
                         call._Duration])
    return outputList
  
  def ShowListOthers(self):
    outputList = []
    for call in self._ListOthers:
      outputList.append([call._Day, 
                         call._Month, 
                         call._Year, 
                         call._Hour, 
                         call._DestinationPhoneNumber,
                         call._Carrier,
                         call._Cost])
    return outputList

  def ShowDictCalls(self):
    outputList = []
    for call in self._ListCalls:
      outputList.append({'Day': call._Day, 
                         'Month': call._Month, 
                         'Year': call._Year, 
                         'Hour': call._Hour, 
                         'DestinationPhoneNumber': call._DestinationPhoneNumber,
                         'Carrier': call._Carrier,
                         'Cost': call._Cost,
                         'Duration': call._Duration})
    return outputList
  
  def ShowDictOthers(self):
    outputList = []
    for call in self._ListOthers:
      outputList.append({'Day': call._Day, 
                         'Month': call._Month, 
                         'Year': call._Year, 
                         'Hour': call._Hour, 
                         'DestinationPhoneNumber': call._DestinationPhoneNumber,
                         'Carrier': call._Carrier,
                         'Cost': call._Cost})
    return outputList
      
          

class Line:
  def __init__(self, parent):
    print("Creating new Line obj...")
    self._Parent = parent
    
    self._CurrentMonth = ""
    self._CurrentYear = ""
    
    self._Alias = ""# Alias de la línea
    self._PhoneNumber = ""
    self._Status = ""# Activo / Inactivo?
    self._SubStatus = ""# ?
    self._BankAccountNumber = ""# Número de la cuenta corriente
    self._Swisda_unk = ""# ?
    self._TipPro_unk = ""# contrato(POS)/prepago(PRE)?
    self._FlatRate = ""# Tarifa plana
    self._FlatRateCost = ""
    self._ContractOrPrepaid = ""
    
    self._MaxTrafficPlan = ""# Tráfico máximo que cubre la tarifa (INT)
    self._DataConsumed = ""# Datos consumidos
    self._PercentageDataConsumed = ""# Más precisión que el que proporciona la API
    
    self._CallDuration = ""# En segundos
    self._CallsCosts = ""# Gasto en llamadas
    
    self._CallDetails = ""
    
    self._Balance = "" # Saldo disponible
    self._Plan = "" # Tarifa contratada
    
    print("Line obj created.")
    
    
  def DownloadCallsDetails(self, InitDate, EndDate):
    request = self._Parent.RequestCallDetails(self._PhoneNumber, InitDate, EndDate)
    print("Request: ", request)
    if request != 0:
      self._CallDetails = request
      return 1
    else:
      return 0
    
  def ReturnLineBasics(self):
    return [self._Alias, 
            self._PhoneNumber, 
            self._Status, 
            self._BankAccountNumber, 
            self._TipPro_unk, 
            self._FlatRate, 
            self._FlatRateCost,
            self._ContractOrPrepaid,
            self._MaxTrafficPlan,
            self._DataConsumed,
            self._PercentageDataConsumed,
            self._CallDuration,
            self._CallsCosts,
            self._Balance,
            self._Plan,
            self._TotalExpenses]

  def ReturnCallsDetails(self):
      return self._CallDetails.ShowListCalls()
  
  def ReturnMsgsDetails(self):
      return self._CallDetails.ShowListOthers()
    
  
class HubLines:
  def __init__(self):
    print("Initiating Hub...")
    self._PhoneNumbers = []
    self._PhoneAliases = []
    self._ContractOrPrepaid = []
    self._Objects = []
    print("Hub initiated.")
  
  def Add(self, NewLine):
    print("Spliting Line data...")
    self._PhoneNumbers.append(NewLine._PhoneNumber)
    self._PhoneAliases.append(NewLine._Alias)
    self._ContractOrPrepaid.append(NewLine._ContractOrPrepaid)# POS para contrato y ¿PRE para prepago?
    self._Objects.append(NewLine)
    print("Line added: ", NewLine._PhoneNumber, NewLine._Alias)
    return 1
  
  def GetAllLines(self):
    return self._Objects
  
  def GetAllPhoneNumbers(self):
    return self._PhoneNumbers
  
  def GetAllPhoneAliases(self):
    return self._PhoneAliases
  
  def Get(self, PhoneNumber = "", Alias = ""):
    if PhoneNumber == "" and Alias == "":
      return 0
    
    elif PhoneNumber != "":
      print("Looking for PhoneNumber: ", PhoneNumber)
      try:
        theIndex = self._PhoneNumbers.index(PhoneNumber)
        return self._Objects[theIndex]
      except:
        return 0
      
    elif Alias != "":
      print("Looking for Alias: ", Alias)
      try:
        theIndex = self._PhoneAliases.index(Alias)
        return self._Objects[theIndex]
      except:
        return 0
      
    else:
      print("Looking for nothing.")
      return 0


class Pepe:
  """ Holds everything for conections and data gathered.
      Is the initial object needed for use the library.
  """
  def __init__(self):
    print("Initiating...")
    # Urls
    self._UrlLogin = "https://servicios.pepephone.com/ppm_appmobile/login"
    self._UrlCallsDetails = "https://servicios.pepephone.com/ppm_appmobile/detalle_llamadas"
    self._UrlServices = "https://servicios.pepephone.com/ppm_appmobile/servicios"
    self._Version = "4.0.1"
    # More to come...
    
    self._UserName = ""
    self._Password = ""
    self._SessionKey = ""
    self._IDClient = ""
    
    # Hub with all the user's lines
    self._Hub = HubLines()
        
  def DirectLogin(self,Username, Password):
    """ Method for login providing username and password.
    """
    self._UserName = Username
    self._Password = Password
    return self.Login()
    
  def Login(self):
    """ Returns 1 if everything is correct, 0 if there is any problem.
        If session is timeout Login method has to be executed to get a new key.
    """
    Parameters = {"p_email": self._UserName, 
                  "p_pwd": self._Password, 
                  "p_verapp": self._Version, 
                  "p_veross": "4"}
    try:
      r = requests.post(self._UrlLogin, params=Parameters)
      #print(r.status_code, r.text)
      if r.status_code == 200:
        root = etree.XML(r.text)
        if root[0].text != '707':
          self._SessionKey = root.attrib['ses']
          self._IDClient = root.attrib['idecli']
          self._SessionStartDate = root[0].text
          self._ClientName = root[1].text
          return 1
        else:
          """ Error 707 means email or password are incorrect.
          """
          return 2
      else:
        """ Conection problem.
        """
        return 3
    except:
      """ Exception raised in the request.
      """
      print("Exception in login!")
      return 0

    
  def RequestServices(self):
    """ Request basic info related to the client.
        This method has to be called in order to request more details.
    """
    print("requesting service data...")
    Parameters = {"key": self._SessionKey}
    try:
      r = requests.post(self._UrlServices, params=Parameters)
      if r.status_code == 200:
        root = etree.XML(r.text)
        return self.SaveServiceData(root)
      else:
        """ Conection problem.
        """
        return 0
    except:
      """ Exception raised in the request.
      """
      return 0
    
  def SaveServiceData(self,rootElement):
    """ This method is not intended for being called directly.
        It's called by RequestServices()
    """
    print("saving service data...")
    for child in rootElement:
      #print(child, child.tag, child.attrib)
      if child.tag == "tipo_linea":
        """ Every child of 'child' object should be a different
            phone number with a contract SIM card.
        """
        print("Lines were found.")
        for contract in child:
          aLine = Line(self)

          aLine._Alias = child[0][1].text # Alias de la línea

          aLine._PhoneNumber = child[0].attrib['msisdn']
          aLine._Status = child[0].attrib['estado'] # Activo / Inactivo?
          aLine._SubStatus = child[0].attrib['subestado'] # ?
          aLine._BankAccountNumber = child[0].attrib['numccc'] # Número de la cuenta corriente
          aLine._Swisda_unk = child[0].attrib['swisda'] # ?
          aLine._TipPro_unk = child[0].attrib['tippro'] # contrato(POS)/prepago(PRE)?
          aLine._FlatRate = child[0].attrib['tplana'] # Tarifa plana
          aLine._FlatRateCost = child[0][6][1].text.replace(".",",")
          aLine._ContractOrPrepaid = child[0].attrib['tippro']
          
          aLine._MaxTrafficPlan = child[0][6][0].text # Tráfico máximo que cubre la tarifa (INT)
          aLine._DataConsumed = child[0][6][3].text # Datos consumidos
          aLine._PercentageDataConsumed = '{0:.2f}'.format(100*float(aLine._DataConsumed.replace(",","."))/float(aLine._MaxTrafficPlan)) # Más precisión que el que proporciona la API
          aLine._CallDuration = child[0][7][0].text # En segundos
          aLine._CallsCosts = child[0][7][2].text # Gasto en llamadas
          aLine._TotalExpenses= str(float(aLine._FlatRateCost.replace(",",".")) + float(aLine._CallsCosts.replace(",","."))).replace(".",",")
          
          aLine._Balance = child[0][4].text
          aLine._Plan = child[0][3].text
          
          print("Adding line...")
          self._Hub.Add(aLine)
      else:
        pass
    return 1
    
  def RequestCallDetails(self, aPhoneNumber, InitDate, EndDate):
    """ This method is not intended for being called directly.
        It's called by each Line object.
    """
    Parameters = {"p_msisdn": aPhoneNumber, 
                  "xsid": self._SessionKey, 
                  "key": self._SessionKey, 
                  "p_fecini": InitDate, 
                  "p_fecfin": EndDate, 
                  "p_orden": "DES", 
                  "p_numpag": "1", 
                  "p_numreg": "1000", 
                  "xres": "C"}
    try:
      r = requests.post(self._UrlCallsDetails, params=Parameters)
      if r.status_code == 200:
        root = etree.XML(r.text)
        print("Requesting details for " + aPhoneNumber + ".")
        TheLine = self._Hub.Get(PhoneNumber = aPhoneNumber)
        return CallDetails(root)
      else:
        """ Conection problem.
        """
        print("Conection problem!")
        return 0
    except:
      """ Exception raised in the request.
      """
      print("Exception problem!")
      return 0
    
    
    
    
  def TestingXML(self):
    import xml.etree.ElementTree as etree
    tree = etree.parse('test.xml')
    root = tree.getroot()
    print(root.attrib['ses'])
    
  def TestingXML2(self):
    import xml.etree.ElementTree as etree
    tree = etree.parse('test2.xml')
    root = tree.getroot()
    print(root)
    print("\n")
    #print(root[1][0].attrib['msisdn'])
    #print(root[1][0][6][0].text)
    #print(root[1][0].attrib['tplana'])

